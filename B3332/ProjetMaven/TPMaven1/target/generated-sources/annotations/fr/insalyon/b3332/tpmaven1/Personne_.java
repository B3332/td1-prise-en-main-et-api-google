package fr.insalyon.b3332.tpmaven1;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-02-10T15:35:49")
@StaticMetamodel(Personne.class)
public class Personne_ { 

    public static volatile SingularAttribute<Personne, String> prenom;
    public static volatile SingularAttribute<Personne, Date> dateNaissance;
    public static volatile SingularAttribute<Personne, String> adresse;
    public static volatile SingularAttribute<Personne, Integer> age;
    public static volatile SingularAttribute<Personne, Long> ID;
    public static volatile SingularAttribute<Personne, Integer> longitude;
    public static volatile SingularAttribute<Personne, Integer> latitude;
    public static volatile SingularAttribute<Personne, String> nom;

}