/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.b3332.tpmaven1;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import java.sql.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author vfalconier
 */
public class JavaTP1 {

    static String nomPersistence = "unitePersistenceTP1";

    public static void init() {
        String nom = "Jean";
        String prenom = "bon";
        int nbGens = 10;
        int ageDefaut = 50;

        EntityManagerFactory emf = Persistence.createEntityManagerFactory(nomPersistence);
        EntityManager em = emf.createEntityManager();

        //On ouvre une transaction
        em.getTransaction().begin();

        for (int i = 0; i < nbGens; i++) {
            //String nom, String prenom, int age, Date dateNaissance, String adresse, int latitude, int longitude
            Personne pers = new Personne(nom + Integer.toString(i), prenom + Integer.toString(i), ageDefaut + i, new Date((long) (System.currentTimeMillis() - Math.random() * 1000000000)), "Adresse", 1, 1);
            em.persist(pers);
        }

        //Ancienne verison
        //Personne pers1 = new Personne("Nom","Prenom",40);
        //em.persist(pers1);
        //On commit
        em.getTransaction().commit();
        em.close();
    }

    public static Personne seekPers(Long id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(nomPersistence);
        EntityManager em = emf.createEntityManager();

        //Recherche d'une personne
        Personne answer = em.find(Personne.class, id);

        return answer;
    }

    public static void affichePers() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(nomPersistence);
        EntityManager em = emf.createEntityManager();

        Query q = em.createNativeQuery("SELECT * FROM PERSONNE", Personne.class);
        List<Personne> resultat = (List<Personne>) q.getResultList();
        for (int i = 0; i < resultat.size(); i++) {
            System.out.println(resultat.get(i));
        }
    }

    final static GeoApiContext MON_CONTEXTE_GEOAPI
            = new GeoApiContext().setApiKey("AIzaSyDcVVJjfmxsNdbdUYeg9MjQoJJ6THPuap4");

    public static com.google.maps.model.LatLng getLatLng(String adresse) {
        try {
            GeocodingResult[] results
                    = GeocodingApi.geocode(MON_CONTEXTE_GEOAPI, adresse).await();
            return results[0].geometry.location;
        } catch (Exception ex) {
            return null;
        }
    }

    public static void main(String[] args) {

        // === Initialisation de la table avec des valeurs == 
        init();

        // === Travail sur la table persistente == 
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(nomPersistence);
        EntityManager em = emf.createEntityManager();

        //On ouvre une transaction
        em.getTransaction().begin();

        //Recherche d'une personne
        System.out.println(seekPers((long) (3)));

        //On affiche toutes les personnes
        affichePers();
        
        //Google API
        String adresse1 = "3 rue rozier, Lyon";
        com.google.maps.model.LatLng coords1 = getLatLng(adresse1);
        System.out.println("Coords: " + coords1.lat + " / " + coords1.lng);

        //On commit
        em.getTransaction().commit();
        em.close();

    }

}
