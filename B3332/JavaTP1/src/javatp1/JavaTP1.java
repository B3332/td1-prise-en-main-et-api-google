/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatp1;

import java.sql.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author vfalconier
 */
public class JavaTP1 {

    public static void init() {
        String nom = "Jean";
        String prenom = "bon";
        int nbGens = 10;
        int ageDefaut = 50;
        

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("unitePersistenceTP1");
        EntityManager em = emf.createEntityManager();

        //On ouvre une transaction
        em.getTransaction().begin();

        for (int i = 0; i < nbGens; i++) {
            //String nom, String prenom, int age, Date dateNaissance, String adresse, int latitude, int longitude
            Personne pers = new Personne(nom + Integer.toString(i), prenom + Integer.toString(i), ageDefaut + i, new Date((long) (System.currentTimeMillis() - Math.random() * 1000000000)), "Adresse", 1, 1);
            em.persist(pers);
        }

        //Ancienne verison
        //Personne pers1 = new Personne("Nom","Prenom",40);
        //em.persist(pers1);
        //On commit
        em.getTransaction().commit();
        em.close();
    }

    public static Personne seekPers(Long id) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("unitePersistenceTP1");
        EntityManager em = emf.createEntityManager();

        //Recherche d'une personne
        Personne answer = em.find(Personne.class, id);

        return answer;
    }

    public static void affichePers() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("unitePersistenceTP1");
        EntityManager em = emf.createEntityManager();

        Query q = em.createNativeQuery("SELECT * FROM PERSONNE", Personne.class);
        List<Personne> resultat = (List<Personne>) q.getResultList();
        for (int i = 0; i < resultat.size(); i++) {
            System.out.println(resultat.get(i));
        }
    }

    public static void main(String[] args) {

        // === Initialisation de la table avec des valeurs == 
        init();

        // === Travail sur la table persistente == 
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("unitePersistenceTP1");
        EntityManager em = emf.createEntityManager();

        //On ouvre une transaction
        em.getTransaction().begin();

        //Recherche d'une personne
        System.out.println(seekPers((long) (3)));

        //On affiche toutes les personnes
        affichePers();

        //On commit
        em.getTransaction().commit();
        em.close();

    }

}
